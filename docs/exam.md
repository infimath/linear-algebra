# Exams

The course has two exams one is internal of 15 marks, while the other is final worth 70 marks. Rest marks are part of continuous assesment that belongs to attendence, seminar and assignments.

## Internal Exam

## Final Exam

Final exam divided in three groups. The questions are organized in three groups as follows:

| Group | No of <br> Questions | Marks per <br> Question | Total |
|-------|:--------------------:|:-----------------------:|-------|
| A     |         10/10        |            2            | 20    |
| B     |         04/05        |            5            | 20    |
| C     |         03/05        |            10           | 30    |